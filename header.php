<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> >
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/src/favicon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="<?php bloginfo('template_url'); ?>/node_modules/video.js/dist/video-js.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/core/css/style_loader.css?ver=0.1.0" />
    <?php wp_head();?>
</head>
<body <?php hybrid_attr('body'); ?> name="top">
<?php
$site = "";
if(is_front_page()){ $site = "primary"; }else{ $site = "secondary"; }
?>
<div id="outer_wrap" class="<?php echo $site ;?>">
<?php get_template_part('wp_setup/sidebars/sidebar-dafault'); ?>
<?php get_template_part('wp_setup/components/svg_container.php'); ?>
<?php get_template_part('wp_setup/menus/menu-primary'); ?>
<?php include(locate_template('wp_setup/components/svg_container.php')); ?>
<div id="inner_wrap">
