jQuery(document).ready(function ($) {
  var logo = $("#logo");
  var prime_slider = $("#prime_slider");
  var videos = $("video");
  var background = $("#background");
  var next_controll = $("#next_controll");
  var intro_title = $(".intro_title");
  var sections = $("section.main");
  simpleParallax("3", prime_slider);
  sections.each(function (i, obj) {

    var top_of_element = $(this).offset().top;
    var half_of_element = $(this).offset().top + ($(this).outerHeight() / 2);

    var bottom_of_element = $(this).offset().top + $(this).outerHeight();

    var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
    var top_of_screen = $(window).scrollTop();

    if (half_of_element >= top_of_screen && half_of_element <= bottom_of_screen) {
      console.log(obj.id, obj, 'top_of_element ' + top_of_element, 'top_of_screen ' + top_of_screen, 'bottom_of_screen ' + bottom_of_screen);
      if (obj.id) $('#ancher_nav a[href*="#' + obj.id + '"].nav-link ').addClass('current');
      $(this).addClass('visible');
      $(this).addClass('seen');
      //  $(this).children().children().fadeIn(2500);
    } else {
      $('#ancher_nav a[href*="#' + obj.id + '"]').removeClass('current');
      $(this).removeClass('visible');
      //  $(this).children().children().fadeOut(2500);
    }
  });


  $(window).scroll(function () {
    sections.each(function (i, obj) {

      var top_of_element = $(this).offset().top;
      var half_of_element = $(this).offset().top + ($(this).outerHeight() / 2);

      var bottom_of_element = $(this).offset().top + $(this).outerHeight();

      var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
      var top_of_screen = $(window).scrollTop();

      if (half_of_element >= top_of_screen && half_of_element <= bottom_of_screen) {

        if (obj.id) $('#ancher_nav a[href*="#' + obj.id + '"].nav-link ').addClass('current');
        $(this).addClass('visible');
        $(this).addClass('seen');
        //  $(this).children().children().fadeIn(2500);
      } else {
        $('#ancher_nav a[href*="#' + obj.id + '"]').removeClass('current');
        $(this).removeClass('visible');
        //  $(this).children().children().fadeOut(2500);
      }
    });
  });
});

function simpleParallax(intensity, element) {
  $(window)
    .scroll(function () {
      var scrollTop = $(window).scrollTop();
      var imgPos = scrollTop / intensity + 'px';
      element.css('transform', 'translateY(' + imgPos + ')');
    });
}

function movebackground(element) {
  var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

  $(window).on('mousemove click', function (e) {
    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
    lFollowY = (10 * lMouseY) / 100;

    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;

    translate = 'translate(' + x + 'px, ' + y + 'px)  scale(1.1)'; //

    $(element).css({
      '-webit-transform': translate,
      '-moz-transform': translate,
      'transform': translate
    });

    //window.requestAnimationFrame(move);
  });

}

function move(element) {
  var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

  $(window).on('mousemove click', function (e) {
    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
    lFollowY = (10 * lMouseY) / 100;

    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;

    translate = 'translate(' + x + 'px, ' + y + 'px)'; // scale(1.1)

    $(element).css({
      '-webit-transform': translate,
      '-moz-transform': translate,
      'transform': translate
    });

    //window.requestAnimationFrame(move);
  });

}
