<div class="section_wrap <?php echo $child_classes; ?>">
    <div class="section_content ">
        <div class="container">
            <?php the_sub_field('textblock'); ?>
        </div>
    </div>
</div>
