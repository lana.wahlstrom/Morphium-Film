<?php
/**
 * Primary Menu Template
 *
 * Displays the Primary Menu if it has active menu items.
 *
 **/

if ( has_nav_menu( 'primary' ) ) : ?>
    <nav id="ancher_nav" class="site-header navbar sticky-top container">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => true, 'menu_class' => 'navbar-nav nav mr-auto ', 'menu_id' => 'menu-primary-items') ); ?>
    </nav>
<?php endif; ?>
