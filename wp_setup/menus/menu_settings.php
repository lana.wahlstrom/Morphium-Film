<?php

/* Register custom menus. */
add_action( 'init', 'complex_register_menus', 5 );
function complex_register_menus() {
	register_nav_menu( 'primary',    _x( 'Primary',    'nav menu location', 'hybrid-base' ) );
	register_nav_menu( 'secondary',  _x( 'Secondary',  'nav menu location', 'hybrid-base' ) );
	register_nav_menu( 'subsidiary', _x( 'Subsidiary', 'nav menu location', 'hybrid-base' ) );
}


function wpdocs_special_nav_class( $classes, $item ) {
	if (in_array('current-menu-item', $classes) ){
		$classes[] = 'nav-item active ';
	}
	$classes[] = "nav-item";
	return $classes;
}
add_filter( 'nav_menu_css_class', 'wpdocs_special_nav_class' , 10, 2 );

function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');


?>