<?php

/**
 * Footer Template
 *
 * The footer template is generally used on every page of your site. Nearly all other
 * templates call it somewhere near the bottom of the file. It is used mostly as a closing
 * wrapper, which is opened with the header.php file. It also executes key functions needed
 * by the theme, child themes, and plugins.
 *
 */
?>



  <!--#main-->
  <footer id="footer" class="main-panel ">
    <div class="container">
      <div class="row">

      <div class="col-12 ">

      
         <ul class="list-unstyled" >
          <li class=" ">
              <i class="fas fa-phone-square"></i>
              <a>030 498 55 3 66</a>
          </li>
          <li class=" ">
              <i class="fas fa-phone-square"></i>
              <a>0176-23936477</a>
          </li>
          <li class=" ">
              <i class="fas fa-envelope-square"></i>
              <a>pool@morphium-film.de</a>
          </li>
        </ul>
      </div>



     </div>
       <div class="row">
        <div class="col-12 ml-sm-auto ">
        <p class="copyright credits">
          <b>Morphium Film</b> |  © <?php echo date('Y'); ?>
        </p>
        <p class="copyright credits">
          Created and Maintained by
          <a href="http://kniessner.com" target="_blank"><b>Kniessner Complex</b></a> | &copy; <?php echo date('Y'); ?>
        </p>
      </div>
    </div>


  </footer>


  </div>
  <!--#innerwrap-->
</div>
<!--#outerwrap-->
<?php include(locate_template('wp_setup/components/modals/modal_video.php')); ?>

<?php include(locate_template('wp_setup/components/modals/modal_contact_form.php')); ?>



<?php wp_footer(); ?>
<script type="text/javascript">
console.log('footer inline sript');
jQuery(document).ready(function ($) {
  var w = window.innerWidth;
  $("#client_slider").slick({
    dots: false,
    centerMode: false,
    infinite: true,

    slidesToShow: 7,
    slidesToScroll:3,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 4
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 2,
          autoplaySpeed: 2000,
          slidesToScroll: 2

        }
      }
    ]
  });
  if(w <= 768){
              $("#loadvideos").slick({
                centerMode: true,
                infinite: true,
                centerPadding: '50px',
                slidesToShow: 1,
                slidesToScroll: 1,

                prevArrow: $('.prev'),
                nextArrow: $('.next')

              });
            }


});
</script>
</body>
</html>