<?php
/* Social Buttons */
?>
            <div class="social_share ">

              <a target="_blank" rel="noopener noreferrer" href="http://delicious.com/post?url=<?php the_permalink(); ?>&amp;title=<?php echo urlencode(get_the_title($id)); ?>"
                title="Bookmark this post at Delicious">Bookmark at Delicious</a>


              <a rel="nofollow" href="http://digg.com/submit?phase=2&amp;url=<?php the_permalink(); ?>" title="Submit this post to Digg">Digg
                this!</a>

              <a target="_blank" rel="noopener noreferrer" href="http://twitter.com/home?status=<?php echo urlencode("
                Currently reading: "); ?><?php the_permalink(); ?>" title="Share this article with your Twitter followers">Tweet
                this!</a>
                <a target="_blank" rel="noopener noreferrer" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php echo urlencode(get_the_title($id)); ?>"
                title="Share this post on Facebook">Share on Facebook</a>


              <a rel="nofollow" href="http://www.stumbleupon.com/submit?url=<?php the_permalink(); ?>&amp;title=<?php echo urlencode(get_the_title($id)); ?>"
                title="Share this post at StumbleUpon">Stumble this!</a>


        
              <a target="_blank" rel="noopener noreferrer" href="http://blinklist.com/index.php?Action=Blink/addblink.php&amp;url=<?php the_permalink(); ?>&amp;Title=<?php echo urlencode(get_the_title($id)); ?>"
                title="Share this post on Blinklist">Blink This!</a>


              <a rel="nofollow" href="http://furl.net/storeIt.jsp?t=<?php echo urlencode(get_the_title($id)); ?>&amp;u=<?php the_permalink(); ?>"
                title="Share this post on Furl">Furl This!</a>


              <a rel="nofollow" href="http://reddit.com/submit?url=<?php the_permalink(); ?>&amp;title=<?php echo urlencode(get_the_title($id)); ?>"
                title="Share this post on Reddit">Share on Reddit</a>
            </div>
