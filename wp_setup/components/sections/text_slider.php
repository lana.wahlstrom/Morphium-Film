


<!-- <div class="container">
  <div class="row justify-content-md-center">
    <div class="col col-lg-2">
      1 of 3
    </div>
    <div class="col-md-auto">
      Variable width content
    </div>
    <div class="col col-lg-2">
      3 of 3
    </div>
  </div>
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col-md-auto">
      Variable width content
    </div>
    <div class="col col-lg-2">
      3 of 3
    </div>
  </div>
</div> -->


    <div class="section_content container">
        <?php $posts = get_sub_field('slider_items'); ?>
        <?php if ($posts) : ?>
            <div class="profile_wrapper justify-content-center align-items-center row justify-content-md-center">
                <?php foreach ($posts as $post) : ?>
                    <?php setup_postdata($post); ?>

                                <?php $fav_movie_title = get_field('lieblingsfilm')[0]['title']; ?>
                                <?php $fav_movie_src = get_field('lieblingsfilm')[0]['url']; ?>

                                <?php $images = get_field('images'); ?>
                                <?php if ( $images && $fav_movie_title ) : ?>
                                          <div class="col col-6 col-lg-3">
                                            <figure class="profile_card">
                                                <img class="card_image hover_image" src="<?php echo $images[1]['sizes']['medium']; ?>" alt="<?php echo $images[1]['alt']; ?>"  />
                                                <img class="card_image front_image " src="<?php echo $images[0]['sizes']['medium']; ?>" alt="<?php echo $images[0]['alt']; ?>"  />

                                              <figcaption class="">
                                                    <div class="info">
                                                    <span class="Jhr"><small>Jahrgang</small> <?php the_field('jahrgang'); ?></span>
                                                    <h3 ><?php the_title(); ?> <big><?php the_field('last_name'); ?></big> </h3>
                                                    <h4 class="sub_title" ><?php the_field('position', $post->ID); ?></h4>
                                                    <p class="fav_movie" ><b>Lieblingsfilm :</b> </br>
                                                        <?php echo $fav_movie_title; ?></br> <a class="link" href="<?php echo $fav_movie_src; ?>"><i class="fas fa-external-link-alt"></i></a>
                                                    </p>
                                                    </div>


                                                </figcaption>
                                            </figure>
                                        </div>


                                <?php endif; ?>


                <?php endforeach; ?>
            </div><!-- card-deck -->
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>

    </div><!-- inner_section -->


<!--
        <div class="card mb-4 box-shadow card_image" >

                        <div class="card-header">
                            <h4 class="my-0 font-weight-normal"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div>

                        <div class="card-body">
                            <h3 class="card-title last_name"><?php the_field('last_name'); ?></h3>
                            <span class="job_bezeichnung"><?php the_field('position'); ?></span>

                        </div>

                    </div>

-->
